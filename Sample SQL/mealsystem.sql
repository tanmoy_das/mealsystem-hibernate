-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 16, 2020 at 11:05 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mealsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `name`) VALUES
(7, 'friday'),
(3, 'monday'),
(1, 'saturday'),
(2, 'sunday'),
(6, 'thursday'),
(4, 'tuesday'),
(5, 'wednesday');

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

CREATE TABLE `foods` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`id`, `name`) VALUES
(6, 'chicken'),
(4, 'ice cream'),
(3, 'mutton'),
(5, 'vegetable curry');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(53),
(53),
(53),
(53);

-- --------------------------------------------------------

--
-- Table structure for table `meal_times`
--

CREATE TABLE `meal_times` (
  `id` int(11) NOT NULL,
  `time` varchar(255) DEFAULT NULL,
  `day_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meal_times`
--

INSERT INTO `meal_times` (`id`, `time`, `day_id`) VALUES
(9, 'breakfast', 1),
(48, 'dinner', 1),
(10, 'lunch', 1),
(13, 'breakfast', 2),
(14, 'lunch', 2),
(17, 'breakfast', 3),
(18, 'lunch', 3),
(21, 'breakfast', 4),
(22, 'lunch', 4),
(25, 'breakfast', 5),
(26, 'lunch', 5),
(29, 'breakfast', 6),
(30, 'lunch', 6),
(33, 'breakfast', 7),
(34, 'lunch', 7);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `meal_time_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `meal_time_id`) VALUES
(49, 'Alternate Menu', 48),
(8, 'general', 9),
(7, 'general', 10),
(12, 'general', 13),
(11, 'general', 14),
(16, 'general', 17),
(15, 'general', 18),
(20, 'general', 21),
(19, 'general', 22),
(24, 'general', 25),
(23, 'general', 26),
(28, 'general', 29),
(27, 'general', 30),
(32, 'general', 33),
(31, 'general', 34),
(47, 'general', 48);

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(11) NOT NULL,
  `maximum_amount` varchar(255) DEFAULT NULL,
  `food_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `maximum_amount`, `food_id`, `menu_id`) VALUES
(35, '2 sp', 5, 7),
(36, '3pc', 6, 7),
(37, '2pc', 3, 8),
(50, '1 cone', 4, 49),
(51, '0.5 cone', 4, 47),
(52, '1 pc', 6, 47);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK3mvv6wcpb471xlp7ffx7sevao` (`name`);

--
-- Indexes for table `foods`
--
ALTER TABLE `foods`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKlmnpdno9yst912bustvp4vuyr` (`name`);

--
-- Indexes for table `meal_times`
--
ALTER TABLE `meal_times`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKiv6t66p2sne0qj60jwbeee9i9` (`day_id`,`time`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKswe5kcv8idh6mji1ug5s9rxq9` (`name`,`meal_time_id`),
  ADD KEY `FKt5vgd01iy1rtui37x8n6v0svi` (`meal_time_id`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKf1d8nfca2fe1s51te7muce31d` (`menu_id`,`food_id`),
  ADD KEY `FK816269p3i41aq91fvnqhb6g2a` (`food_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `meal_times`
--
ALTER TABLE `meal_times`
  ADD CONSTRAINT `FKdwhbvx0mm97b25tl8k94w9rcy` FOREIGN KEY (`day_id`) REFERENCES `days` (`id`);

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `FKt5vgd01iy1rtui37x8n6v0svi` FOREIGN KEY (`meal_time_id`) REFERENCES `meal_times` (`id`);

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `FK6fwmu1a0d0hysfd3c00jxyl2c` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`),
  ADD CONSTRAINT `FK816269p3i41aq91fvnqhb6g2a` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
