package net.therap.mealsystem;

import net.therap.mealsystem.controller.App;
import net.therap.mealsystem.controller.HibernatedApp;

import java.sql.SQLException;

/**
 * @author tanmoy.das
 * @since 3/5/20
 */
public class Main {

    public static void main(String[] args) throws SQLException {
        App app = null;

        try {

//            app = new JdbcApp(); //Uncomment to use JDBC instead
            app = new HibernatedApp();
            app.initialize();
            app.run(args);

        } catch (SQLException e) {

            System.err.format("SQL State: %s%n%s", e.getSQLState(), e.getMessage());
            e.printStackTrace();

        } finally {

            if (app != null) {
                app.shutdown();
            }

        }
    }
}
