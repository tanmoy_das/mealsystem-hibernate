package net.therap.mealsystem.domain;

import javax.persistence.*;

/**
 * @author tanmoy.das
 * @since 3/5/20
 */
@Entity
@Table(name = "foods", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
public class Food {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    public Food() {
    }

    public Food(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Food(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Food{" +
                "name='" + name + '\'' +
                '}';
    }
}
