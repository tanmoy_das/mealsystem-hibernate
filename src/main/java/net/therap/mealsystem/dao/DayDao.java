package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public interface DayDao {
    Optional<Day> get(int id) throws SQLException;

    List<Day> getAll() throws SQLException;

    Optional<Day> saveOrUpdate(Day day) throws SQLException;

    Optional<Day> save(Day day) throws SQLException;

    Optional<Day> getByName(String dayName) throws SQLException;

    void initializeDays() throws SQLException;
}
