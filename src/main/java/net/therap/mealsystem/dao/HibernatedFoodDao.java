package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Food;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class HibernatedFoodDao implements FoodDao {

    private Session session;

    public HibernatedFoodDao(Session session) {
        this.session = session;
    }

    @Override
    public Optional<Food> get(int id) {
        Food food = session.get(Food.class, id);
        return Optional.ofNullable(food);
    }

    @Override
    public List<Food> getAll() {
        TypedQuery<Food> query = session.createQuery("FROM Food", Food.class);

        return query.getResultList();
    }

    public Optional<Food> saveOrUpdate(Food food) {
        session.saveOrUpdate(food);
        return Optional.ofNullable(food);
    }

    @Override
    public Optional<Food> save(Food food) {
        session.save(food);
        return Optional.of(food);
    }

    @Override
    public Optional<Food> getByName(String foodName) {
        Query query = session.createQuery("FROM Food WHERE name = :name");
        query.setParameter("name", foodName);

        Food food = (Food) query.getSingleResult();
        return Optional.ofNullable(food);
    }

    @Override
    public void update(Food food) {
        session.update(food);
    }

    @Override
    public void updateAll(List<Food> foods) {
        for (Food food : foods) {
            update(food);
        }
    }

    @Override
    public void delete(Food food) {
        session.delete(food);
    }
}
