package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.exceptions.InvalidMealTimeException;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/12/20
 */
public interface MenuDao {
    Optional<Menu> getFirst(MealTime mealTime) throws SQLException, InvalidMealTimeException;

    List<Menu> getAll() throws SQLException;

    List<Menu> getByDay(Day day) throws SQLException;

    void save(Menu menu) throws SQLException;

    void update(Menu menu) throws SQLException;

    void delete(Menu menu) throws SQLException;
}
