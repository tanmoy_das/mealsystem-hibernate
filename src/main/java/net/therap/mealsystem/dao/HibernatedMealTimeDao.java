package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import org.hibernate.Session;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class HibernatedMealTimeDao implements MealTimeDao {

    private Session session;

    public HibernatedMealTimeDao(Session session) {
        this.session = session;
    }

    @Override
    public Optional<MealTime> get(int id) {
        MealTime mealTime = session.get(MealTime.class, id);
        return Optional.ofNullable(mealTime);
    }

    @Override
    public List<MealTime> getAll() {
        TypedQuery<MealTime> query = session.createQuery("FROM MealTime", MealTime.class);
        return query.getResultList();
    }

    @Override
    public Optional<MealTime> save(MealTime mealTime) {
        session.save(mealTime);
        return Optional.of(mealTime);
    }

    @Override
    public Optional<MealTime> getByDayTime(Day day, String time) {
        Query query = session.createQuery("FROM MealTime mealTime WHERE mealTime.day = :day and time = :time");
        query.setParameter("day", day);
        query.setParameter("time", time);

        try {
            MealTime mealTime = (MealTime) query.getSingleResult();
            return Optional.of(mealTime);
        } catch (NoResultException e) {
            return Optional.empty();
        }


    }

    public List<MealTime> getByDay(Day day) {
        TypedQuery<MealTime> query = session.createQuery("FROM MealTime WHERE day = :day", MealTime.class);
        query.setParameter("day", day);

        return query.getResultList();
    }

    @Override
    public void update(MealTime mealTime) {
        session.update(mealTime);
    }

    @Override
    public void updateAll(List<MealTime> mealTimes) {
        for (MealTime mealTime : mealTimes) {
            update(mealTime);
        }
    }

    @Override
    public void delete(MealTime mealTime) {
        session.delete(mealTime);
    }
}
