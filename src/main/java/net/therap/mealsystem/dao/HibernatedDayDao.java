package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class HibernatedDayDao implements DayDao {

    private Session session;

    public HibernatedDayDao(Session session) {
        this.session = session;
    }

    @Override
    public Optional<Day> get(int id) {
        Day day = session.get(Day.class, id);
        return Optional.ofNullable(day);
    }

    @Override
    public List<Day> getAll() {
        TypedQuery<Day> query = session.createQuery("FROM Day", Day.class);

        return query.getResultList();
    }

    @Override
    public Optional<Day> saveOrUpdate(Day day) {
        session.saveOrUpdate(day);
        return Optional.of(day);
    }

    @Override
    public Optional<Day> save(Day day) {
        session.save(day);
        return Optional.of(day);
    }

    @Override
    public Optional<Day> getByName(String dayName) {
        Query query = session.createQuery("FROM Day WHERE name = :name");
        query.setParameter("name", dayName);

        Day day = (Day) query.getSingleResult();
        return Optional.ofNullable(day);
    }

    @Override
    public void initializeDays() {
        String[] dayNames = Day.DAY_NAMES;

        List<Day> days = new ArrayList<>();
        for (int i = 0; i < dayNames.length; i++) {
            days.add(new Day(i + 1, dayNames[i]));
        }

        for (Day day : days) {
            if (session.get(Day.class, day.getId()) == null) {
                saveOrUpdate(day);
            }
        }
    }
}
