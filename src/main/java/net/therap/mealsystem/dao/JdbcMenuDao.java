package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.domain.MenuItem;
import net.therap.mealsystem.mapper.MenuMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public class JdbcMenuDao implements MenuDao {

    private Connection connection;
    private MenuMapper menuMapper;
    private MealTimeDao mealTimeDao;
    private JdbcMenuItemDao menuItemDao;

    public JdbcMenuDao(Connection connection, MenuMapper menuMapper, MealTimeDao mealTimeDao, JdbcMenuItemDao menuItemDao) {
        this.connection = connection;
        this.menuMapper = menuMapper;
        this.mealTimeDao = mealTimeDao;
        this.menuItemDao = menuItemDao;
    }

    @Override
    public Optional<Menu> getFirst(MealTime mealTime) throws SQLException {
        String sqlSelect = "SELECT * FROM menuItems WHERE mealTimeId = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
        preparedStatement.setInt(1, mealTime.getId());

        ResultSet resultSet = preparedStatement.executeQuery();

        return menuMapper.map(resultSet);
    }

    @Override
    public List<Menu> getAll() throws SQLException {
        List<Menu> menus = new ArrayList<>();

        List<MealTime> mealTimes = mealTimeDao.getAll();
        mealTimes.sort(Comparator.comparingInt(MealTime :: getId));

        for (MealTime mealTime : mealTimes) {
            Optional<Menu> optionalMenu = getFirst(mealTime);
            optionalMenu.ifPresent(menus :: add);
        }

        return menus;
    }

    @Override
    public List<Menu> getByDay(Day day) throws SQLException {
        List<Menu> allMenus = getAll();

        ArrayList<Menu> menus = new ArrayList<>();
        for (Menu menu : allMenus) {
            if (menu.getMealTime().getDay().getName().equals(day.getName())) {
                menus.add(menu);
            }
        }

        return menus;
    }

    @Override
    public void save(Menu menu) throws SQLException {
        for (MenuItem menuItem : menu.getMenuItems()) {
            menuItemDao.save(menu.getMealTime(), menuItem);
        }
    }

    @Override
    public void update(Menu menu) throws SQLException {
        for (MenuItem menuItem : menu.getMenuItems()) {
            menuItemDao.update(menu.getMealTime(), menuItem);
        }
    }

    @Override
    public void delete(Menu menu) throws SQLException {
        List<MenuItem> menuItems = menu.getMenuItems();
        for (MenuItem menuItem : menuItems) {
            menuItemDao.delete(menuItem);
        }
        mealTimeDao.delete(menu.getMealTime());
    }
}
