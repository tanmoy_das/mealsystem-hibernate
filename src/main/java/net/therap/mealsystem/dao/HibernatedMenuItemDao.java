package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.MenuItem;
import org.hibernate.Session;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class HibernatedMenuItemDao {

    private final Session session;

    public HibernatedMenuItemDao(Session session) {
        this.session = session;
    }

    public Optional<MenuItem> get(int id) {
        MenuItem menuItem = session.get(MenuItem.class, id);
        return Optional.ofNullable(menuItem);
    }

    public List<MenuItem> getAll() {
        TypedQuery<MenuItem> query = session.createQuery("FROM MenuItem", MenuItem.class);
        return query.getResultList();
    }


    public Optional<MenuItem> save(MenuItem menuItem) {
        session.save(menuItem);
        return Optional.ofNullable(menuItem);
    }

    public void update(MenuItem menuItem) {
        session.update(menuItem);
    }

    public void delete(MenuItem menuItem) {
        session.delete(menuItem);
    }
}
