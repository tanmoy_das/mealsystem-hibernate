package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/12/20
 */
public interface MealTimeDao {
    Optional<MealTime> get(int id) throws SQLException;

    List<MealTime> getAll() throws SQLException;

    Optional<MealTime> save(MealTime mealTime) throws SQLException;

    Optional<MealTime> getByDayTime(Day day, String time) throws SQLException;

    void update(MealTime mealTime) throws SQLException;

    void updateAll(List<MealTime> mealTimes) throws SQLException;

    void delete(MealTime mealTime) throws SQLException;

    List<MealTime> getByDay(Day day) throws SQLException;
}
