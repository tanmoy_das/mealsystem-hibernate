package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.exceptions.InvalidMealTimeException;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class HibernatedMenuDao implements MenuDao {

    private final Session session;
    private MealTimeDao mealTimeDao;

    public HibernatedMenuDao(Session session, MealTimeDao mealTimeDao) {
        this.session = session;
        this.mealTimeDao = mealTimeDao;
    }

    public Optional<Menu> get(int id) {
        Menu menu = session.get(Menu.class, id);
        return Optional.ofNullable(menu);
    }

    @Override
    public Optional<Menu> getFirst(MealTime mealTime) throws SQLException, InvalidMealTimeException {
        Optional<MealTime> optionalMealTime = mealTimeDao.getByDayTime(mealTime.getDay(), mealTime.getTime());
        if (!optionalMealTime.isPresent()) {
            throw new InvalidMealTimeException();
        }

        mealTime = optionalMealTime.get();

        Query query = session.createQuery("FROM Menu WHERE mealTime = :mealTime");
        query.setParameter("mealTime", mealTime);

        Menu menu = (Menu) query.getSingleResult();
        return Optional.ofNullable(menu);
    }

    @Override
    public List<Menu> getAll() {
        TypedQuery<Menu> query = session.createQuery("FROM Menu", Menu.class);

        return query.getResultList();
    }

    @Override
    public List<Menu> getByDay(Day day) throws SQLException {
        List<MealTime> mealTimes = mealTimeDao.getByDay(day);

        TypedQuery<Menu> query = session.createQuery("FROM Menu menu WHERE menu.mealTime IN (:mealTimes)", Menu.class);
        query.setParameter("mealTimes", mealTimes);

        return query.getResultList();
    }

    @Override
    public void save(Menu menu) {
        session.save(menu);
    }

    @Override
    public void update(Menu menu) {
        session.update(menu);
    }

    @Override
    public void delete(Menu menu) {
        session.delete(menu);
    }
}
