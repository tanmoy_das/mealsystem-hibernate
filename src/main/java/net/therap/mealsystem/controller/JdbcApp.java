package net.therap.mealsystem.controller;

import net.therap.mealsystem.dao.*;
import net.therap.mealsystem.domain.*;
import net.therap.mealsystem.helper.MigrationHelper;
import net.therap.mealsystem.mapper.FoodMapper;
import net.therap.mealsystem.mapper.MealTimeMapper;
import net.therap.mealsystem.mapper.MenuItemMapper;
import net.therap.mealsystem.mapper.MenuMapper;
import net.therap.mealsystem.util.MySqlConnectionManager;
import net.therap.mealsystem.view.FoodViewer;
import net.therap.mealsystem.view.JdbcMenuViewer;
import net.therap.mealsystem.view.MenuViewer;
import net.therap.mealsystem.view.Viewer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
@Deprecated
public class JdbcApp extends App {

    private Connection connection;

    private FoodDao foodDao;
    private MealTimeDao mealTimeDao;
    private JdbcMenuItemDao menuItemDao;
    private MenuDao menuDao;


    public JdbcApp() throws SQLException {
        this.connection = MySqlConnectionManager.createConnection();
        this.foodDao = new JdbcFoodDao(connection, new FoodMapper());
        this.mealTimeDao = new JdbcMealTimeDao(connection, new MealTimeMapper());
        this.menuItemDao = new JdbcMenuItemDao(connection, new MenuItemMapper(foodDao));
        this.menuDao = new JdbcMenuDao(connection, new MenuMapper(mealTimeDao, menuItemDao), mealTimeDao, menuItemDao);
    }

    @Override
    public void run(String[] args) throws SQLException {
        if (connection != null) {
            super.run(args);
        } else {
            System.err.println("Failed to make connection!");
        }
    }

    @Override
    public void refreshAllMigrations() throws SQLException {
        MigrationHelper.refreshAll(connection);
    }

    @Override
    public void rollbackAllMigrations() throws SQLException {
        MigrationHelper.rollbackAll(connection);
    }

    @Override
    public void migrateAllMigrations() throws SQLException {
        MigrationHelper.migrateAll(connection);
    }

    @Override
    public void shutdown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Override
    public void showMenu() throws SQLException {
        List<Menu> menuItems;
        menuItems = menuDao.getAll();
        menuItems.sort(Comparator.comparingInt(Menu :: getId));

        MenuViewer viewer = new JdbcMenuViewer(foodDao, mealTimeDao);
        viewer.view(menuItems);
    }

    @Override
    public void showMenu(String dayName) throws SQLException {
        Day day = new Day(dayName);
        List<Menu> menuItems;
        menuItems = menuDao.getByDay(day);

        MenuViewer viewer = new JdbcMenuViewer(foodDao, mealTimeDao);
        viewer.view(menuItems, new Day[] {day});
    }

    @Override
    public void removeItem(String foodName, String dayName, String time) throws SQLException {
        Day day = new Day(dayName);
        if (!foodDao.getByName(foodName).isPresent()) {
            System.err.println(foodName + " is not in food list. It won't be added to menu.");
        } else {
            if (!mealTimeDao.getByDayTime(day, time).isPresent()) {
                System.err.println("There is no meal at that specified time");
            } else {
                MealTime mealTime = mealTimeDao.getByDayTime(day, time).get();
                Food food = foodDao.getByName(foodName).get();

                Optional<MenuItem> potentialMenuItem;
                potentialMenuItem = menuItemDao.getMenuItemByData(mealTime.getId(), food.getId());
                if (potentialMenuItem.isPresent()) {
                    menuItemDao.delete(potentialMenuItem.get());
                } else {
                    System.err.println(foodName + " is not in the menu at the specified time");
                }
            }
        }
    }

    @Override
    public void updateItem(String foodName, String dayName, String time, String maxAmount) throws SQLException {
        Day day = new Day(dayName);
        if (!foodDao.getByName(foodName).isPresent()) {
            System.err.println(foodName + " is not in food list. It won't be added to menu.");
            //                            foodDao.save(new Food(foodName));
        } else {
            if (!mealTimeDao.getByDayTime(day, time).isPresent()) {
                MealTime mealTime = new MealTime(day, time);
                mealTimeDao.save(mealTime);
            }

            MealTime mealTime = mealTimeDao.getByDayTime(day, time).get();
            Food food = foodDao.getByName(foodName).get();

            Optional<MenuItem> potentialMenuItem;
            potentialMenuItem = menuItemDao.getMenuItemByData(mealTime.getId(), food.getId());
            if (potentialMenuItem.isPresent()) {
                MenuItem menuItem = potentialMenuItem.get();
                menuItem.setMaximumAmount(maxAmount);
                menuItemDao.update(mealTime, menuItem);
            } else {
                System.err.println("Item not in menu at specified time.");
            }
        }
    }

    @Override
    public void addItem(String foodName, String dayName, String time, String maxAmount) throws SQLException {
        Day day = new Day(dayName);
        if (!foodDao.getByName(foodName).isPresent()) {
            System.out.println(foodName + " is not in food list. It won't be added to menu.");
            //                            foodDao.save(new Food(foodName));
        } else {
            if (!mealTimeDao.getByDayTime(day, time).isPresent()) {
                MealTime mealTime = new MealTime(day, time);
                mealTimeDao.save(mealTime);
            }

            MealTime mealTime = mealTimeDao.getByDayTime(day, time).get();
            Food food = foodDao.getByName(foodName).get();

            if (menuItemDao.getMenuItemByData(mealTime.getId(), food.getId()).isPresent()) {
                System.err.println("Item already in menu, not added");
            } else {
                MenuItem menuItem = new MenuItem(mealTime.getId(), food, maxAmount);
                menuItemDao.save(mealTime, menuItem);
            }
        }
    }

    @Override
    public void viewFoods() throws SQLException {
        List<Food> foods = foodDao.getAll();
        foods.sort(Comparator.comparingInt(Food :: getId));

        Viewer<Food> viewer = new FoodViewer();
        viewer.view(foods);
    }

    @Override
    public void deleteFood(String foodName) throws SQLException {
        Optional<Food> potentialFood = foodDao.getByName(foodName);
        if (potentialFood.isPresent()) {
            Food food = potentialFood.get();
            foodDao.delete(food);
        } else {
            System.err.println(foodName + " does not exist in list");
        }
    }

    @Override
    public void addFood(String foodName) throws SQLException {
        Food food = new Food(foodName);
        if (foodDao.getByName(foodName).isPresent()) {
            System.err.println(foodName + " already exists in food list");
        } else {
            foodDao.save(food);
        }
    }
}
