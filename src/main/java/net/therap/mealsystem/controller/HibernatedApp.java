package net.therap.mealsystem.controller;

import net.therap.mealsystem.dao.*;
import net.therap.mealsystem.domain.*;
import net.therap.mealsystem.util.HibernateUtil;
import net.therap.mealsystem.view.FoodViewer;
import net.therap.mealsystem.view.HibernatedMenuViewer;
import net.therap.mealsystem.view.MenuViewer;
import net.therap.mealsystem.view.Viewer;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class HibernatedApp extends App {

    private SessionFactory sessionFactory;
    private Session session;

    private FoodDao foodDao;
    private MealTimeDao mealTimeDao;
    private HibernatedMenuItemDao menuItemDao;
    private MenuDao menuDao;
    private DayDao dayDao;

    public HibernatedApp() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
        this.session = sessionFactory.openSession();

        this.foodDao = new HibernatedFoodDao(session);
        this.mealTimeDao = new HibernatedMealTimeDao(session);
        this.menuItemDao = new HibernatedMenuItemDao(session);
        this.menuDao = new HibernatedMenuDao(session, mealTimeDao);
        this.dayDao = new HibernatedDayDao(session);
    }

    @Override
    public void initialize() throws SQLException {
        session.beginTransaction();
        dayDao.initializeDays();

        session.getTransaction().commit();
    }

    @Override
    public void run(String[] args) throws SQLException {
        session.beginTransaction();

        super.run(args);
        try {
            CommandLine cmd = parseArgs(args);


            if (cmd.hasOption("addMealTime")) {
                if (cmd.hasOption("day") && cmd.hasOption("time")) {
                    String dayName = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");

                    addMealTime(dayName, time);
                } else {
                    System.err.println("Please provide Day and Time");
                }
            }

            if (cmd.hasOption("deleteMealTime")) {
                if (cmd.hasOption("day") && cmd.hasOption("time")) {
                    String dayName = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");

                    deleteMealTime(dayName, time);
                } else {
                    System.err.println("Please provide Day and Time");
                }
            }

            if (cmd.hasOption("addMenu")) {
                if (cmd.hasOption("day") && cmd.hasOption("time") && cmd.hasOption("menuName")) {
                    String dayName = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");
                    String menuName = cmd.getOptionValue("menuName");

                    addMenu(dayName, time, menuName);
                } else {
                    System.err.println("Please provide Day and Time");
                }
            }

            if (cmd.hasOption("deleteMenu")) {
                if (cmd.hasOption("day") && cmd.hasOption("time") && cmd.hasOption("menuName")) {
                    String dayName = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");
                    String menuName = cmd.getOptionValue("menuName");

                    deleteMenu(dayName, time, menuName);
                } else {
                    System.err.println("Please provide Day and Time");
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        session.getTransaction().commit();
    }

    private void addMenu(String dayName, String time, String menuName) throws SQLException {
        Optional<Day> optionalDay = dayDao.getByName(dayName);

        if (!optionalDay.isPresent()) {
            System.err.println("Invalid Day Name");
        } else {
            Day day = optionalDay.get();
            Optional<MealTime> optionalMealTime = mealTimeDao.getByDayTime(day, time);

            if (!optionalMealTime.isPresent()) {
                System.err.println("Invalid Meal Time");
            } else {
                MealTime mealTime = optionalMealTime.get();

                Menu menu = new Menu();
                menu.setName(menuName);
                menu.setMealTime(mealTime);

                List<Menu> menuList = menuDao.getAll();
                for (Menu listedMenu : menuList) {
                    if (menu.getMealTime().equals(listedMenu.getMealTime())
                            && menu.getName().equals(listedMenu.getName())) {
                        System.err.println("Duplicate Menu found");
                        return;
                    }
                }

                mealTime.getMenus().add(menu);

                mealTimeDao.update(mealTime);
                menuDao.save(menu);
            }
        }
    }

    private void deleteMenu(String dayName, String time, String menuName) throws SQLException {
        Optional<Day> optionalDay = dayDao.getByName(dayName);

        if (!optionalDay.isPresent()) {
            System.err.println("Invalid Day Name");
        } else {
            Day day = optionalDay.get();
            Optional<MealTime> optionalMealTime = mealTimeDao.getByDayTime(day, time);

            if (!optionalMealTime.isPresent()) {
                System.err.println("Invalid Meal Time");
            } else {
                MealTime mealTime = optionalMealTime.get();

                Menu menu = null;

                List<Menu> menuList = menuDao.getAll();
                for (Menu listedMenu : menuList) {
                    if (mealTime.toString().equals(listedMenu.getMealTime().toString())
                            && menuName.equals(listedMenu.getName())) {
                        menu = listedMenu;
                    }
                }

                if (menu != null) {
                    mealTime.getMenus().remove(menu);

                    for (MenuItem menuItem : menu.getMenuItems()) {
                        menuItemDao.delete(menuItem);
                    }
                    menu.getMenuItems().clear();

                    mealTimeDao.update(mealTime);
                    menuDao.delete(menu);
                }
            }
        }
    }

    private void addMealTime(String dayName, String time) throws SQLException {
        Optional<Day> optionalDay = dayDao.getByName(dayName);

        if (!optionalDay.isPresent()) {
            System.err.println("Invalid Day Name");
        } else {
            Day day = optionalDay.get();
            MealTime mealTime = new MealTime(day, time);
            day.getMealTimes().add(mealTime);

            Menu menu = new Menu();
            menu.setMealTime(mealTime);
            menu.setName(Menu.DEFAULT_NAME);

            mealTime.getMenus().add(menu);

            menuDao.save(menu);
            mealTimeDao.save(mealTime);
            dayDao.saveOrUpdate(day);
        }
    }

    private void deleteMealTime(String dayName, String time) throws SQLException {
        Optional<Day> optionalDay = dayDao.getByName(dayName);

        if (!optionalDay.isPresent()) {
            System.err.println("Invalid Day Name");
        } else {
            Day day = optionalDay.get();
            Optional<MealTime> optionalMealTime = mealTimeDao.getByDayTime(day, time);

            if (!optionalMealTime.isPresent()) {
                System.err.println("Invalid Meal Time");
            } else {
                MealTime mealTime = optionalMealTime.get();
                day.getMealTimes().remove(mealTime);

                List<Menu> menuList = mealTime.getMenus();
                for (Menu menu : menuList) {
                    for (MenuItem menuItem : menu.getMenuItems()) {
                        menuItemDao.delete(menuItem);
                    }
                    menuDao.delete(menu);
                }

                dayDao.saveOrUpdate(day);
                mealTimeDao.delete(mealTime);
            }
        }
    }

    @Override
    public void addBreakfastLunch() throws SQLException {
        List<Day> days = dayDao.getAll();
        days.sort(Comparator.comparingInt(Day :: getId));

        for (Day day : days) {
            MealTime breakfast = new MealTime(day, "breakfast");
            MealTime lunch = new MealTime(day, "lunch");

            Menu menu = new Menu();
            menu.setName(Menu.DEFAULT_NAME);
            menu.setMealTime(lunch);
            lunch.getMenus().add(menu);

            Menu menu1 = new Menu();
            menu1.setName(Menu.DEFAULT_NAME);
            menu1.setMealTime(breakfast);
            breakfast.getMenus().add(menu1);

            day.getMealTimes().clear();
            day.getMealTimes().add(breakfast);
            day.getMealTimes().add(lunch);

            menuDao.save(menu);
            menuDao.save(menu1);

            dayDao.saveOrUpdate(day);

            session.saveOrUpdate(breakfast);
            session.saveOrUpdate(lunch);
        }
    }

    @Override
    @Deprecated
    public void migrateAllMigrations() {
    }

    @Override
    @Deprecated
    public void rollbackAllMigrations() {
    }

    @Override
    @Deprecated
    public void refreshAllMigrations() {
    }

    @Override
    public void showMenu() throws SQLException {
        List<Menu> menus = menuDao.getAll();
        menus.sort(Comparator.comparingInt(Menu :: getId));

        MenuViewer menuViewer = new HibernatedMenuViewer(dayDao);
        menuViewer.view(menus);
    }

    @Override
    public void showMenu(String dayName) throws SQLException {
        Optional<Day> optionalDay = dayDao.getByName(dayName);
        if (!optionalDay.isPresent()) {
            System.err.println("Day name invalid");
        } else {
            Day day = optionalDay.get();
            List<Menu> menus = menuDao.getByDay(day);
            MenuViewer menuViewer = new HibernatedMenuViewer(dayDao);
            menuViewer.view(menus, new Day[] {day});
        }
    }

    public Optional<Menu> findMeal(String dayName, String time, String mealName) throws SQLException {
        Optional<Day> optionalDay = dayDao.getByName(dayName);
        if (!optionalDay.isPresent()) {
            System.err.println("Day name invalid");
        } else {
            Day day = optionalDay.get();
            Optional<MealTime> optionalMealTime = mealTimeDao.getByDayTime(day, time);

            if (!optionalMealTime.isPresent()) {
                System.err.println("Invalid Menu Time");
            } else {
                MealTime mealTime = optionalMealTime.get();
                List<Menu> menus = mealTime.getMenus();

                for (Menu menu : menus) {
                    if (menu.getName().equals(mealName)) {
                        return Optional.of(menu);
                    }
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public void removeItem(String foodName, String dayName, String time) throws SQLException {
        removeItem(foodName, dayName, time, Menu.DEFAULT_NAME);
    }

    @Override
    public void removeItem(String foodName, String dayName, String time, String mealName) throws SQLException {
        Optional<Menu> optionalMeal = findMeal(dayName, time, mealName);

        if (!optionalMeal.isPresent()) {
            System.err.println("Menu doesn't exist at specified time");
        } else {
            Menu menu = optionalMeal.get();


            MenuItem menuItemToDelete = null;
            for (MenuItem menuItem : menu.getMenuItems()) {
                if (menuItem.getFood().getName().equals(foodName)) {
                    menuItemToDelete = menuItem;
                }
            }

            if (menuItemToDelete == null) {
                System.err.println("Item not in the menu at specified time");
            } else {
                menu.getMenuItems().remove(menuItemToDelete);
                menuDao.update(menu);
                menuItemDao.delete(menuItemToDelete);
            }
        }
    }

    @Override
    public void updateItem(String foodName, String dayName, String time, String maxAmount) throws SQLException {
        updateItem(foodName, dayName, time, maxAmount, Menu.DEFAULT_NAME);
    }

    @Override
    public void updateItem(String foodName, String dayName, String time, String maxAmount, String mealName) throws SQLException {
        Optional<Menu> optionalMeal = findMeal(dayName, time, mealName);

        if (!optionalMeal.isPresent()) {
            System.err.println("Menu doesn't exist at specified time");
        } else {
            Menu menu = optionalMeal.get();

            MenuItem menuItemToUpdate = null;
            for (MenuItem menuItem : menu.getMenuItems()) {
                if (menuItem.getFood().getName().equals(foodName)) {
                    menuItemToUpdate = menuItem;
                }
            }

            if (menuItemToUpdate == null) {
                System.err.println("Item does not exist in the specific menu");
            } else {
                menuItemToUpdate.setMaximumAmount(maxAmount);
                menuItemDao.update(menuItemToUpdate);
            }
        }
    }

    @Override
    public void addItem(String foodName, String dayName, String time, String maxAmount) throws SQLException {
        addItem(foodName, dayName, time, maxAmount, Menu.DEFAULT_NAME);
    }

    @Override
    public void addItem(String foodName, String dayName, String time, String maxAmount, String mealName) throws SQLException {
        Optional<Menu> optionalMeal = findMeal(dayName, time, mealName);

        if (!optionalMeal.isPresent()) {
            System.err.println("Menu doesn't exist at specified time");
        } else {
            Menu menu = optionalMeal.get();

            Optional<Food> optionalFood = foodDao.getByName(foodName);
            if (!optionalFood.isPresent()) {
                System.err.println("Invalid Food name");
            } else {
                Food food = optionalFood.get();

                for (MenuItem menuItem : menu.getMenuItems()) {
                    if (menuItem.getFood().getName().equals(food.getName())) {
                        System.err.println("Food already in menu. " +
                                "To update the maximum amount use --updateItem option");
                        return;
                    }
                }

                MenuItem menuItem = new MenuItem(food, maxAmount);
                menu.getMenuItems().add(menuItem);
                menuItem.setMenu(menu);

                menuItemDao.save(menuItem);
                menuDao.update(menu);
            }
        }
    }

    @Override
    public void viewFoods() throws SQLException {
        List<Food> foods = foodDao.getAll();
        foods.sort(Comparator.comparingInt(Food :: getId));

        Viewer<Food> viewer = new FoodViewer();
        viewer.view(foods);
    }

    @Override
    public void deleteFood(String foodName) throws SQLException {
        Optional<Food> optionalFood = foodDao.getByName(foodName);
        if (!optionalFood.isPresent()) {
            System.err.println("Invalid Food Name");
        } else {
            Food food = optionalFood.get();
            foodDao.delete(food);
        }
    }

    @Override
    public void addFood(String foodName) throws SQLException {
        Food food = new Food(foodName);
        foodDao.save(food);
    }

    @Override
    public void shutdown() {
        if (session != null && session.isOpen()) {
            session.close();
        }

        if (sessionFactory != null && !sessionFactory.isClosed()) {
            sessionFactory.close();
        }
    }
}
