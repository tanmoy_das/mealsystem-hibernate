package net.therap.mealsystem.util;

import org.apache.commons.cli.*;


/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class CliParser {
    private Options options;

    public CliParser() {
        this.options = new Options();
    }

    public CliParser(Options options) {
        this.options = options;
    }

    public void addOption(String name, boolean hasArg, String description) {
        addOption(name, name, hasArg, description);
    }

    public void addOption(String name, String longName, boolean hasArg, String description) {
        Option addFoodOption = new Option(name, longName, hasArg, description);
        options.addOption(addFoodOption);
    }

    public void addOption(Option option) {
        options.addOption(option);
    }

    public CommandLine parseCliArguments(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();

        try {
            return parser.parse(options, args);
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(" gradle -q run --args='[Option] [target] [Option] [target]...'", options);
            throw e;
        }

    }
}
