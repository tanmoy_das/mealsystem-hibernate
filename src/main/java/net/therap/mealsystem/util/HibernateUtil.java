package net.therap.mealsystem.util;

import net.therap.mealsystem.domain.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class HibernateUtil {
    private static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory() {
        try {
            Configuration configuration = new Configuration();

            Properties props = new Properties();
            props.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
            props.put("hibernate.connection.url", "jdbc:mysql://127.0.0.1:3306/mealsystem");
            props.put("hibernate.connection.username", "tanmoy");
            props.put("hibernate.connection.password", "");
            props.put("hibernate.current_session_context_class", "thread");

            props.put("hibernate.show_sql", "false");
            props.put("hibernate.hbm2ddl.auto", "update");

            configuration.setProperties(props);

            configuration.addAnnotatedClass(Day.class);
            configuration.addAnnotatedClass(Food.class);
            configuration.addAnnotatedClass(MealTime.class);
            configuration.addAnnotatedClass(Menu.class);
            configuration.addAnnotatedClass(MenuItem.class);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            System.out.println("Hibernate Java Config serviceRegistry created");

            return configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) sessionFactory = buildSessionFactory();
        return sessionFactory;
    }
}
