package net.therap.mealsystem.mapper;

import net.therap.mealsystem.domain.Food;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/10/20
 */
public class FoodMapper {
    public Optional<Food> map(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            int id = resultSet.getInt("id");

            String name = resultSet.getString("name");

            return Optional.of(new Food(id, name));

        } else {
            return Optional.empty();
        }
    }
}
