package net.therap.mealsystem.exceptions;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class InvalidMealTimeException extends Exception {
    public InvalidMealTimeException() {
        super("Mealtime does not exist in the database");
    }
}
