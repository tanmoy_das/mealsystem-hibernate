package net.therap.mealsystem.view;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.Menu;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class JdbcMenuViewer implements MenuViewer {

    private FoodDao foodDao;
    private MealTimeDao mealTimeDao;

    public JdbcMenuViewer(FoodDao foodDao, MealTimeDao mealTimeDao) {
        this.foodDao = foodDao;
        this.mealTimeDao = mealTimeDao;
    }

    @Override
    public void view(Menu item) throws SQLException {
        ArrayList<Menu> items = new ArrayList<>();
        items.add(item);
        view(items);
    }

    @Override
    public void view(List<Menu> menus) throws SQLException {
        String[] dayNames = Day.DAY_NAMES;

        Day[] days = new Day[dayNames.length];
        for (int i = 0; i < dayNames.length; i++) {
            days[i] = new Day(dayNames[i]);
        }
        view(menus, days);
    }

    @Override
    public void view(List<Menu> menus, Day[] days) {
        Map<String, List<Menu>> routines = new HashMap<>();
        for (Day day : days) {
            routines.put(day.getName(), new ArrayList<>());
        }

        for (Menu menu : menus) {
            String dayName = menu.getMealTime().getDay().getName();
            routines.get(dayName).add(menu);
        }

        for (Day day : days) {
            System.out.println(day.getName());
            System.out.println("---------------------");

            for (Menu menu : routines.get(day.getName())) {
                System.out.print(menu.getMealTime().getTime());
                System.out.print(": ");
                for (int i = 0; i < menu.getMenuItems().size(); i++) {
                    if (i > 0) {
                        System.out.print(", ");
                    }
                    String foodName = menu.getMenuItems().get(i).getFood().getName();
                    String maxAmount = menu.getMenuItems().get(i).getMaximumAmount();
                    System.out.printf("%s (%s)", foodName, maxAmount);
                }
                System.out.println();
            }

            System.out.println();
            System.out.println();
        }
    }
}
