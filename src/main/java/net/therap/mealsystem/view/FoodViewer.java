package net.therap.mealsystem.view;

import net.therap.mealsystem.domain.Food;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class FoodViewer implements Viewer<Food> {

    @Override
    public void view(List<Food> foods) {
        System.out.println("---------------------");
        System.out.println("|  id|          name|");
        System.out.println("---------------------");
        for (Food food : foods) {
            System.out.printf("|%4d|%14s|%n", food.getId(), food.getName());
        }
        System.out.println("---------------------");
    }

    @Override
    public void view(Food item) {
        List<Food> items = new ArrayList<>();
        items.add(item);
        view(items);
    }
}
