package net.therap.mealsystem.migration;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public interface Migratable {

    void migrate(Connection connection) throws SQLException;

    void rollback(Connection connection) throws SQLException;


    default void refresh(Connection connection) throws SQLException {
        migrate(connection);
        rollback(connection);
        migrate(connection);
    }
}
