package net.therap.mealsystem.migration;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class DayMigration implements Migratable {
    @Override
    public void migrate(Connection connection) throws SQLException {
        String sqlCreate = "create table days (id integer not null, name varchar(20), primary key (id))";
        connection.prepareStatement(sqlCreate).execute();
    }

    @Override
    public void rollback(Connection connection) throws SQLException {
        String sqlDrop = "DROP TABLE days";
        connection.prepareStatement(sqlDrop).execute();
    }
}
