import net.therap.mealsystem.domain.*;
import net.therap.mealsystem.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class HibernatedEntityTest {
    private SessionFactory sessionFactory;

    public HibernatedEntityTest() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static void main(String[] args) {
        HibernatedEntityTest test = new HibernatedEntityTest();

        test.initialize();

        try {
            test.run(args);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        test.shutdown();
    }

    public void initialize() {
        addDays();
    }

    private void addDays() {
        String[] dayNames = new String[] {"saturday", "sunday", "monday", "tuesday", "wednesday", "thursday", "friday"};
        List<Day> days = new ArrayList<>();

        for (int i = 0; i < dayNames.length; i++) {
            days.add(new Day(i + 1, dayNames[i]));
        }


        Session session = sessionFactory.openSession();
        session.beginTransaction();
        for (Day day : days) {
            session.saveOrUpdate(day);
        }
        session.getTransaction().commit();
        session.close();
    }

    public void run(String[] args) throws SQLException {
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        Menu menu1 = new Menu();
        Menu menu2 = new Menu();

        Day day = session.get(Day.class, 1);

        MealTime mealTime1 = new MealTime(day, "Lunch");
        MealTime mealTime2 = new MealTime(day, "breakfast");

        day.getMealTimes().add(mealTime1);
        day.getMealTimes().add(mealTime2);

        menu1.setMealTime(mealTime1);
        mealTime1.getMenus().add(menu1);

        menu2.setMealTime(mealTime2);
        mealTime2.getMenus().add(menu2);


        Food chicken = new Food("chicken");
        Food mutton = new Food("mutton");

        Food fish = new Food("fish");
        Food vegetables = new Food("vegetables");


        MenuItem menuItem1 = new MenuItem(chicken, "2pc");
        MenuItem menuItem2 = new MenuItem(mutton, "3pc");

        MenuItem menuItem3 = new MenuItem(fish, "1pc");
        MenuItem menuItem4 = new MenuItem(vegetables, "unlimited");

        menu1.getMenuItems().add(menuItem1);
        menu1.getMenuItems().add(menuItem2);


        menuItem1.setMenu(menu1);
        menuItem2.setMenu(menu1);

        menu2.getMenuItems().add(menuItem3);
        menu2.getMenuItems().add(menuItem4);

        menuItem3.setMenu(menu2);
        menuItem4.setMenu(menu2);

        session.saveOrUpdate(mealTime1);
        session.saveOrUpdate(mealTime2);


        session.saveOrUpdate(chicken);
        session.saveOrUpdate(mutton);

        session.saveOrUpdate(fish);
        session.saveOrUpdate(vegetables);


        session.saveOrUpdate(menuItem1);
        session.saveOrUpdate(menuItem2);

        session.saveOrUpdate(menuItem3);
        session.saveOrUpdate(menuItem4);

        session.saveOrUpdate(menu1);
        session.saveOrUpdate(menu2);

        session.getTransaction().commit();
        session.close();

        session = sessionFactory.openSession();
        session.beginTransaction();
        day = session.get(Day.class, 1);

        List<MealTime> mealTimes = day.getMealTimes();

        System.out.println();
        System.out.println();
        for (MealTime mealTime : mealTimes) {
            List<Menu> menus = mealTime.getMenus();
            for (Menu menu : menus) {
                System.out.println(menu);
                System.out.println();
            }
        }
        System.out.println();
        System.out.println();
    }

    public void shutdown() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }
}
